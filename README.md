# TCL Talks - Webpack Example Repo

This repo goes along with the slides from the [TCL Webpack Talk](https://docs.google.com/presentation/d/15_2qpTNa9yCh71-sVgjyOVrpD3iq96DU0EUIKxS__ng/edit?usp=sharing). These packages provide examples for using and learning Webpack. Each module has a README explaining the concept of each example.

We start with a project with no webpack at all, and gradually add more features to the build.

## Examples TOC:

0. [No webpack](https://gitlab.com/stevegardner/tcl-webpack/blob/b98a0417102a2eb45e3be63748041fe69a2f3ad2/packages/00-no-webpack)
1. [No-cofig webpack](https://gitlab.com/stevegardner/tcl-webpack/blob/3d0c647d7f0e806c66e4b14b037c28dbe14ef5eb/packages/01-basic-webpack)
1. [Basic config](https://gitlab.com/stevegardner/tcl-webpack/blob/3d0c647d7f0e806c66e4b14b037c28dbe14ef5eb/packages/02-basic-webpack-config)
1. [Manage Assets](https://gitlab.com/stevegardner/tcl-webpack/blob/85b9ae6fc92ec48c3ad8494248b1ee72adc09c8e/packages/03-manage-assets)
1. [Plugins](https://gitlab.com/stevegardner/tcl-webpack/blob/85b9ae6fc92ec48c3ad8494248b1ee72adc09c8e/packages/04-plugins)
1. [Dev-Server](https://gitlab.com/stevegardner/tcl-webpack/blob/85b9ae6fc92ec48c3ad8494248b1ee72adc09c8e/packages/05-dev-server)
1. [React](https://gitlab.com/stevegardner/tcl-webpack/blob/85b9ae6fc92ec48c3ad8494248b1ee72adc09c8e/packages/06-react)
1. [Development vs. Produdction settings](https://gitlab.com/stevegardner/tcl-webpack/blob/cd735a67852c21b1292b74421e1e6f9cacc8be42/packages/07-dev-vs-prod)
