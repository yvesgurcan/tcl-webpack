#!/usr/bin/env bash


echo $1
if [[ $# -eq 0 ]]
then
  echo $'***\nYou need to specify a workspace dir within ./packages\n***\n'
  exit 1
fi

node node_modules/.bin/live-server packages/$1