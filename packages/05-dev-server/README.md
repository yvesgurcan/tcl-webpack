# 05-dev-server

Webpacks built in dev-server is great for development. Like all things Webpack, its quite configurable as well. We‘re going to look at the built-in dev-server now. Definitely checkout building out a custom dev-server using Express as well.

## Source Maps

One thing that we often will see in development is source maps. If we try to debug code that Webpack has compiled, it's a bit obfuscated. The code is hard to read and jumbled into a single line. Source maps provide a link to the source code. From there, we can inspect the code in the browser, as well as drop breakpoints where we'd expect.

We use `eval-source-map` to get quality source maps for our compiled code, as recommended by Webpack for development. Different `devtool` settings have different tradeoffs in speed and quality. In production, we usually don't want to return source maps. But, it is advantageous if the server can be cofigured to allow source maps for staff users.
