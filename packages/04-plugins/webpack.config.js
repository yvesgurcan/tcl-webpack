const path = require('path');
const webpack = require('webpack');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'build'),
  },
  plugins: [
    // Ensures we cleanup the output directory on each build
    new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
    // Creates an index.html file in the output directory using public/index.html as a template.
    // Our bundles will be automatically appended to the body tag.
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'public', 'index.html'),
    }),
    // Does what it says. We specify files to copy into our output directory.
    // Here, we copy over everything except index.html in our /public folder.
    new CopyPlugin({
      patterns: [
        {
          from: 'public',
          globOptions: { ignore: ['index.html'] },
        },
      ],
    }),
    // Add to process.env. These values are resolved at build time and optimized by Webpack.
    new webpack.EnvironmentPlugin({
      GREETING: 'hello',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all',
        },
      },
    },
  },
};
