# 04-plugins

Plugins tweak how webpack runs. We‘re going to add a few plugins to make our development smoother.

## CleanWebpackPlugin

This plugin ensures we have a clean output folder when the build runs. If we happen to change the name of a bundle or make another tweak to Webpack, we'll only see the files that are relevant to the most recent build.

## CopyPlugin

By default, copies files we specify to the output directory. In this example, we ignore `index.html` in the public folder since `HtmlWebpackPlugin` takes care of `index.html` for us.

## HtmlWebpackPlugin

This creates the `index.html` file that we need to load the project in the browser. This is a huge win for us, becuase we can now add or modify chunk names without having to manually update script tags. Those bundle script tags are appended automatically.

Even better is that we can provide a template. The parts of `index.html` we want to remain consistent can be written in the template. Then Webpack will still append the bundle script tags to `body` for us.

## WebpackEnvironmentPlugin

We can define values to be as environment variables within our app. This is a `node` convention that Webpack gives us in the build step. Browsers don't know what `process.env` is.

Webpack gives us `process.env.NODE_ENV` out of the box. We can then add to those values. At build time, those values are resolved by the compiler.
