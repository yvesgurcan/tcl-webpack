import _ from 'lodash';

import './message.css';

const message = document.createElement('p');
const text = `${process.env.GREETING} from a webpack bundle`;

console.log('NODE_ENV: ', process.env.NODE_ENV);

message.textContent = _.capitalize(text);
document.querySelector('body').append(message);
