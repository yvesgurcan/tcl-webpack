const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'build'),
    // Path where files are served from the server
    publicPath: '/build/',
    // We are overriding the default name to group images in a separate folder.
    // Later we'll configure development to use the actual filename instead of the hash.
    assetModuleFilename: 'images/[hash][ext]',
  },
  module: {
    rules: [
      {
        // Match an file that ends with a .css extension.
        test: /\.css$/,
        // Order is important. Starting from index -1,
        // results are passed down the chain.
        use: ['style-loader', 'css-loader'],
      },
      {
        // Match any files with .jpg extension.
        test: /\.jpg$/,
        // Special field for assets.
        // https://webpack.js.org/guides/asset-modules/
        // asset chooses between a data URI (asset/inline) and the actual file (asset/resource).
        // This used to require separate loaders, but is now built into Webpack v5.
        type: 'asset',
      },
    ],
  },
  // Splits out common chunks from the main bundle
  optimization: {
    splitChunks: {
      cacheGroups: {
        // We're splitting out all node_modules code within this bundle. We're naming it `vendor`.
        // `vendor` is also a key we give the group. It could be anything.
        vendor: {
          // Like `module.rules` configurations, test here defines which files will grouped together.
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          // Using `all`
          chunks: 'all',
        },
      },
    },
  },
};
