import { capitalize } from 'lodash';

import './message.css';

const message = document.createElement('p');
const text = 'hello from a webpack bundle';

message.textContent = capitalize(text);
document.querySelector('body').append(message);
