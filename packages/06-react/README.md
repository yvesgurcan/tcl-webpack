# 06-react

Adding React with JSX to a project takes a few additional steps. While webpack is kind of a compiler, it's specific to creating bundles. It doesn't transpile code itself. In the case of React, we need a real compile to use JSX. JSX isn't a browser standard language. A tool like [babel](https://babeljs.io/) will take React JSX and transpile it to `React.createElement`. As of [React 17](https://reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html), React now transforms JSX to `_jsx`, which eliminates the need to have React in scope for components.

Once we have `babel` cofigured to handle JSX, we can tell Webpack to look out for JSX or JS files and use the `babel-loader` to transform JSX in any `.js` or `.jsx` files.

```js
{
  // Match any .js or .jsx files
  test: /\.(js|jsx)$/,
  // Ignore node_modules since they should already be built or ready to use.
  exclude: /node_modules/,
  // transpile matched files with babel-loader. It will use .babelrc for configuration.
  use: ['babel-loader'],
},
```

## Source Maps

Here, we defined that we want source maps in development, but not in production. In the next example we'll look at making more distinctions between a development and production build.
