import _ from 'lodash';

const message = document.createElement('p');
const text = 'hello from a webpack bundle';

message.textContent = _.capitalize(text);
document.querySelector('body').append(message);
