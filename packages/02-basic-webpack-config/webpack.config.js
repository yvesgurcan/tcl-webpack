const path = require('path');

module.exports = {
  // The file which pulls in all the JS for the app. It is the dependency tree ancestor.
  entry: './src/index.js',
  // What gets spit out by Webpack and where does it go.
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
